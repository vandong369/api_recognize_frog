# from operator import mod
import tensorflow as tf
from tensorflow import keras
from heapq import nlargest
from keras.preprocessing import image
import numpy as np
import flask
from flask import request
import base64
from PIL import Image
from io import BytesIO
from heapq import nlargest

model = tf.keras.models.load_model("doggy_v4.h5")

size = (256, 256)

class_names = [
    "Mountain Chicken Frog",
    "Archey’s Frogs",
    "Oregon Spotted Frog",
    "Baw Baw Frog",
    "Panamanian Golden Frog",
    "Corroboree Frog",
    "Kihansi Spray Toad",
    "Lemur Leaf Frog",
    "Golden Mantella",
    "Lake Titicaca Frog",
    "Spike-Thumb Frog",
    "Yellow Spotted Bell Frog",
    "Puerto Rican Crested Toad",
    "Pelophylax Lessonae"
]


def load_image(img_base64):
    im = b64_2_img(img_base64)
    im = image.smart_resize(im, size, interpolation="bilinear")

    img_tensor = image.img_to_array(im)  # (height, width, channels)
    img_tensor = np.expand_dims(
        img_tensor, axis=0
    )  # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)

    return img_tensor


def b64_2_img(data):
    buff = BytesIO(base64.b64decode(data))
    return Image.open(buff)


def detect_mushroom(img_data):
    img_array = load_image(img_data)
    predictions = model.predict(img_array)
    score = tf.nn.softmax(predictions[0])
    largestRs = nlargest(
        3, range(len(predictions.reshape(14))), predictions.reshape(14).take
    )

    rs = class_names[largestRs[0]] + "|" + str(score[largestRs[0]].numpy())
    if score[largestRs[1]] > 0.05:
        rs = (
            rs
            + "#"
            + class_names[largestRs[1]]
            + "|"
            + str(score[largestRs[1]].numpy())
        )
    if score[largestRs[2]] > 0.05:
        rs = (
            rs
            + "#"
            + class_names[largestRs[2]]
            + "|"
            + str(score[largestRs[2]].numpy())
        )
    print(rs)
    return rs


app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route("/detect", methods=["POST"])
def detect():
    base64DataOfImage = request.json["image"]
    return detect_mushroom(base64DataOfImage)


app.run()
