import { NextFunction, Request } from 'express';
import { isCelebrate } from 'celebrate';
import httpStatus from 'http-status';
import ResponseContext from '../interfaces/responseWithHelper.interface';
import ApiError from "./api-error";

export default function(req: Request, res: ResponseContext, next: NextFunction) {
    res.success = function(status = 200, data: any) {
        return this.status(status).json(data).end();
    };

    res.error = function(err: any) {
        if (process.env.NODE_ENV === 'development') console.error(err);

        let responseError = err;

        if (isCelebrate(err)) {
            responseError = err;
            responseError.status = httpStatus.BAD_REQUEST;
        } else if (!(err instanceof ApiError)) {
            responseError = new ApiError(err.message, err.status);
        }
        return this.status(responseError.status).json({ message: responseError.message, status: responseError.status }).end();
    };

    return next();
}
