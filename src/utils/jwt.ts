import jwt from 'jsonwebtoken';
import { get } from 'lodash';
import { Request } from 'express';

import { JWT_SECRET } from '../configs';

export const sign = async (payload: any, expiresIn = 2592000): Promise<string> => jwt.sign(
        payload,
        JWT_SECRET,
        {
            expiresIn,
        },
    );

export const verify = async (token: string): Promise<any> => jwt.verify(token, JWT_SECRET);

export const generateResetPasswordToken = async (payload: any, expiresIn = 600): Promise<string> => jwt.sign(
    payload,
    JWT_SECRET,
    {
        expiresIn,
    },
);

export const getToken = (req: Request): string => {
    const token = get(req, 'query.token');
    if (token) {
        return token;
    }
    if (req.headers) {
        const authorization = get(req, 'headers.authorization');
        if (!authorization) {
            return '';
        }
        const tokens = authorization.split('Bearer ');
        return get(tokens, '[1]');
    }
    return '';
};
