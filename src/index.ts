import dotEnv from 'dotenv';

dotEnv.config();
import App from './app';
import DogRoute from './handlers/dog/dog.route';

const app = new App(
    [
        new DogRoute(),
    ],
);

app.listen();
