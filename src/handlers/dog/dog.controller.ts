import httpStatus = require('http-status');
import { NextFunction } from 'express';
import { get, set } from 'lodash';
import IRequestWithUser from '../../interfaces/requestWithUser.interface';
import IResponse from '../../interfaces/responseWithHelper.interface';
import { DogInfo } from '../../utils';
import axios from 'axios';
import dogModel from './dog.model';
// var FormData = require('form-data');
// var fs = require('fs');

export default class DogController {
    public getDogInfo = async (req: any, res: IResponse, next: NextFunction): Promise<any> => {
        try {
            const data: any = {
                image: req.file.buffer.toString('base64')
            }
            var config: any = {
                method: 'post',
                url: 'http://127.0.0.1:5000/detect',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            };

            const rs = await axios(config);
            // console.log("data res: ",rs.data)
            console.log(rs.data);
            const results = await DogInfo.getDogInfo(rs.data);

            const result = {
                "error_code": 0,
                "error_message": "",
                "data": {
                    results
                }
            }
            return res.success(httpStatus.OK, result);
        } catch (err) {
            console.log(err);
            return next(err);
        }
    };

    public getDogs = async (req: IRequestWithUser, res: IResponse, next: NextFunction): Promise<any> => {
        try {
            const search = req.query.q as string;
            const page = parseInt(req.query.page as string, 10) || 0;
            const limit = 15;

            const where: any = {}


            if (search && search != '') {
                const searchRegex: string = `.*${search}.*`;
                where.name = {
                    $regex: searchRegex,
                    $options: 'i',
                }
            }

            const skip = limit && page ? limit * page : 0;

            const [dogs, totalRecord] = await Promise.all([
                dogModel.find(where).skip(skip).limit(limit).select('_id name image intro').lean(),
                dogModel.countDocuments(where)
            ])
            // const dogs = await dogModel.find(where).skip(skip).limit(limit).select('_id name image intro').lean();

            const hasNextPage = skip + dogs.length < totalRecord ? true : false;
            const addNumber = totalRecord % limit == 0 ? 0 : 1;
            const totalPage = Math.floor(totalRecord / limit) + addNumber;

            return res.success(httpStatus.OK, {
                success: true,
                data: dogs,
                hasNextPage,
                totalPage
            });
        } catch (err) {
            return next(err);
        }
    };

    public getDog = async (req: IRequestWithUser, res: IResponse, next: NextFunction): Promise<any> => {
        try {
            const dogId = req.params.dogId;
            if (!dogId) return res.error(httpStatus.BAD_REQUEST);

            const dogInDB = await dogModel.findOne({ _id: dogId }).lean();
            if (!dogInDB) return res.error(httpStatus.NOT_FOUND);

            return res.success(httpStatus.OK, {
                success: true,
                data: dogInDB,
            });
        } catch (err) {
            return next(err);
        }
    };

    public getDogDisease = async (req: IRequestWithUser, res: IResponse, next: NextFunction): Promise<any> => {
        try {
            const dogDisease = DogInfo.getDogDisease();
            return res.success(httpStatus.OK, {
                success: true,
                data: dogDisease,
            });
        } catch (err) {
            console.log(err);
            return next(err);
        }
    };
}
