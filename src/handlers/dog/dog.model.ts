import mongoose from 'mongoose';
import { IDogDocument } from './dog.interface';

export const dogSchema = new mongoose.Schema(
    {
        name: String,
        image: String,
        intro: String,
        // detail: [Object],
        // vitalStats: [Object],
        // more: [Object],
    }, { timestamps: true },
);

const dogModel = mongoose.model<IDogDocument>('Dogs', dogSchema);

export default dogModel;
