import mongoose from 'mongoose';

export interface IDog {
    _id?: string;
    name: string;
    image: string;
    intro: string[],
    // detail: Object[],
    // vitalStats: Object[],
    // more: Object[],
}

export interface IDogDocument extends IDog, mongoose.Document {
    _id: string;
}
