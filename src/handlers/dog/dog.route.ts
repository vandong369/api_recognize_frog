import { Router } from 'express';
import IController from '../../interfaces/controller.interface';
import DogController from './dog.controller';
import multer from 'multer';
import validationMiddleware from '../../middlewares/validation.middleware';
import { Joi } from 'celebrate';

class DogRoute implements IController {
    public router = Router();
    private uploadImage = multer();

    private dogController = new DogController();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.post(
            '/detectDog',
            this.uploadImage.single('image'),
            this.dogController.getDogInfo,
        );

        this.router.get(
            '/dogs',
            validationMiddleware({
                query: {
                    q: Joi.string().allow('').allow(null),
                    page: Joi.number().integer().min(0),
                }
            }),
            this.dogController.getDogs,
        );

        this.router.get(
            '/dogs/:dogId',
            this.dogController.getDog,
        );

        this.router.get(
            '/dogDisease',
            this.dogController.getDogDisease,
        );
    }
}

export default DogRoute;