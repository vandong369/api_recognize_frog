import bodyParser from 'body-parser';
import express from 'express';
import expressPino from 'express-pino-logger';
import cors from 'cors';
import { middleware } from 'x-hub-signature';
import responseHandler from './utils/response';
import errorMiddleware from './middlewares/error.middleware';
import mongoose from 'mongoose';
import { DB_URI } from './configs';
import { crawlFrogInfo } from './crawlers/crawl-frog-info'

class App {
    public app: express.Application;

    constructor(route: any[]) {
        this.app = express();

        this.connectToDatabase().then(() => {
            this.initializeMiddleware();
            this.initializeRoutes(route);
            this.initializeErrorHandling();
            // this.insertDatabase();
        }).catch((err) => {
            console.log('Could not connect to DB', err);
        });
    }

    public listen() {
        this.app.listen(process.env.PORT, () => {
            console.log(`App listening on the port ${process.env.PORT}`);
        });
    }

    private initializeMiddleware() {
        this.app.use((req, res, next) => {
            bodyParser.json({
                verify: middleware.extractRawBody,
            })(req, res, (err) => {
                if (err) {
                    return res.status(400).json({ error: 'Bad json' });
                }
                next();
            });
        });
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(expressPino({ level: 'info' }));
        this.app.use(responseHandler);
        this.app.use(cors({
            origin: '*',
        }));
    }

    private initializeErrorHandling() {
        this.app.use(errorMiddleware);
    }

    private initializeRoutes(controllers: any[]) {
        controllers.forEach((controller) => {
            this.app.use('/api', controller.router);
        });
    }

    private connectToDatabase() {
        return mongoose.connect(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });
    }

    private insertDatabase() {
        // crawlFrogInfo()
    }
}

export default App;
