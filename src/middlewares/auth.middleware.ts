import httpStatus from 'http-status';
import { NextFunction } from 'express';
import { isString, startsWith } from 'lodash';
import jwt from 'jsonwebtoken';
import ApiError from '../utils/api-error';
import IRequestWithUser from '../interfaces/requestWithUser.interface';
import { JWT_SECRET } from '../configs';

interface IDataStoredInToken {
    _id: string;
}

async function authMiddleware(req: IRequestWithUser, res, next: NextFunction) {
    const token = req.headers.authorization;

    if (!isString(token) || !startsWith(token, 'Bearer ')) {
        return next(new ApiError('AUTHENTICATION_FAILED', httpStatus.UNAUTHORIZED));
    }

    try {
        const signedToken = token.split('Bearer ')[1];
        const verificationResponse = await jwt.verify(signedToken, JWT_SECRET) as IDataStoredInToken;
        req.user = verificationResponse;

        return next();
    } catch (err) {
        return next(new ApiError('AUTHENTICATION_FAILED', httpStatus.UNAUTHORIZED));
    }
}

export default authMiddleware;
