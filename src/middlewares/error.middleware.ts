function errorMiddleware(err: any, req: any, res: any, next: any) {
    return res.error(err);
}

export default errorMiddleware;
