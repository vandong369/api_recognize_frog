import axios from 'axios';
import { JSDOM } from "jsdom";
import fs from 'fs';

const catUrls = [
    { url: "https://cattime.com/cat-breeds/sphynx-cats", name: "Sphynx" },
];

async function crawlData(url) {
    for (const catUrl of catUrls) {
        const data = await axios.get(catUrl.url);
        var dom = new JSDOM(data.data);

        const result: any = {
            name: catUrl.name
        }

        const image = dom.window.document.querySelector('img.breed-featured-img').getAttribute('data-lazy-src');
        // console.log(image);
        result.image = image;

        const intro = [];
        dom.window.document.querySelectorAll('div.breeds-single-intro p').forEach(async (doc: any) => {
            intro.push(doc.textContent);
        });
        result.intro = intro;

        const detail = [];
        dom.window.document.querySelectorAll('div.breed-characteristics-ratings-wrapper.paws').forEach(async (doc: any) => {
            const item: any = {}
            // item.name = doc.querySelector('div.characteristic-stars.parent-characteristic h2').textContent;
            item.name = 'test';
            const indicators = [];
            doc.querySelectorAll('div.js-list-item.child-characteristic a').forEach(async (childDoc: any) => {
                if (childDoc.querySelector('div.characteristic-title')) {
                    indicators.push({
                        name: childDoc.querySelector('div.characteristic-title').textContent,
                        stars: childDoc.querySelector('div.characteristic-star-block div').textContent
                    })
                }
            });
            item.indicators = indicators;
            detail.push(item);
        });
        result.detail = detail;

        const vitalStats = [];
        dom.window.document.querySelectorAll('div.vital-stat-box').forEach(async (doc: any) => {
            const temp = doc.textContent.split(':');
            if (temp[0] == 'Life Span') {
                vitalStats.push({
                    image: 'https://cattime.com/wp-content/themes/cattime-2019/images/icons/icon-breed-lifespan.svg',
                    key: temp[0],
                    value: temp[1]
                })
            } else if (temp[0] == 'Length') {
                vitalStats.push({
                    image: 'https://cattime.com/wp-content/themes/cattime-2019/images/icons/icon-breed-height.svg',
                    key: temp[0],
                    value: temp[1]
                })
            } else if (temp[0] == 'Weight') {
                vitalStats.push({
                    image: 'https://cattime.com/wp-content/themes/cattime-2019/images/icons/icon-breed-weight.svg',
                    key: temp[0],
                    value: temp[1]
                })
            } else if (temp[0] == 'Origin') {
                vitalStats.push({
                    image: 'https://cattime.com/wp-content/themes/cattime-2019/images/icons/icon-breed-group.svg',
                    key: temp[0],
                    value: temp[1]
                })
            }
        });
        result.vitalStats = vitalStats;

        const more = [];
        dom.window.document.querySelectorAll('li.breed-data-item.js-accordion-item.item-expandable-content').forEach(async (doc: any) => {
            const item: any = {}
            if (doc.querySelector('h3').textContent == '') {
                item.name = 'More About This Breed';
            } else {
                item.name = doc.querySelector('h3').textContent
            };

            const content = [];
            doc.querySelectorAll('p').forEach(async (childDoc: any) => {
                content.push(childDoc.textContent);
            });
            item.content = content;
            more.push(item);
        })
        result.more = more;
        fs.appendFile('cat-infos.json', JSON.stringify(result) + '\n', function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log("Done", catUrl);
            }
        })
    }
}

crawlData(catUrls);