import axios from "axios";
import { JSDOM } from "jsdom";
import fs from "fs";
import dogModel from "../handlers/dog/dog.model";

const url =
  "https://www.google.com/search?q=Pelophylax+lessonae&sxsrf=APq-WBs33EFkmPHpZ7zX23P7UxKtMSBemw:1649926927071&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjOzZLjmJP3AhXKCd4KHb0tAx4Q_AUoAXoECAMQAw&biw=1179&bih=686&dpr=1.25";
const name = "Pelophylax-lessonae";

const jsonFilePath = `D:/.Foxcode/projects/indentifier-backend-sample/indentifier-backend-sample/src/crawlers/frog-img.json`;

async function crawlData(url) {
  const data = await axios.get(url);
  const dom = new JSDOM(data.data);
  let temp = 1;
  // console.log(data.data);

  dom.window.document
    .querySelectorAll("div.NZWO1b")
    .forEach(async (doc: any) => {
      const imgUrl = doc.querySelector("img").getAttribute("src");

      const fileName = `${name}${temp}`;

      // fs.appendFile(jsonFilePath, "\n" + JSON.stringify(imgUrl), function (err) {
      //   if (err) throw err;
      //   // console.log(JSON.stringify(ringtone));
      // });

      axios.get(imgUrl, { responseType: "arraybuffer" }).then((response) => {
        fs.writeFile(
          `D:/.Foxcode/work-file/frog-img/${name}/${fileName}.jpg`,
          response.data,
          (err) => {
            if (err) throw err;
            console.log("The file has been saved! ", name);
          }
        );
      });

      temp++;
      if (temp === 50) return false;
      else return true;
    });
}



export async function crawlFrogInfo() {
  const wikiUrl =
    "https://www.waza.org/blog/13-frog-species-zoos-and-aquariums-are-helping-to-conserve-in-the-fight-against-extinction/";
  const dataRes = await axios.get(wikiUrl);
  const dom = new JSDOM(dataRes.data);
  
  //crawl name
  let frogName = []
  dom.window.document.querySelectorAll("h2.elementor-heading-title").forEach(async (doc: any) => {
    const name = doc.textContent.trim().slice(3).trim()
    frogName.push(name)
  })
  console.log("name: ", frogName.length);

  //crawl image url 
  let frogImg = []
  dom.window.document.querySelectorAll("figure.wp-caption img").forEach(async (doc: any) => {
    const imgUrl = doc.getAttribute("src")
    frogImg.push(imgUrl);
  })
  console.log("img: ", frogImg.length);
  // console.log("img: ", frogImg);


  //crawl intro
  let frogIntro = [];
  let temp = 1
  dom.window.document.querySelectorAll("div.elementor-text-editor p").forEach(async (doc: any) => {
    const intro = doc.textContent
    const checking = "Fun fact"
    if (temp > 6 && temp < 32 && intro.indexOf(checking) === -1) frogIntro.push(intro);
    temp++
  })
  console.log("intro: ", frogIntro.length);


  for (let i = 0; i < 13; i++) {
    let frog = {
      name: frogName[i],
      image: frogImg[i],
      intro: frogIntro[i]
    }
  
    // const frog14 = {
    //   name: "Lake Titicaca Frog",
    //   image:
    //     "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Vodnice_posv%C3%A1tn%C3%A1_zoo_praha_1.jpg/330px-Vodnice_posv%C3%A1tn%C3%A1_zoo_praha_1.jpg",
    //   intro:
    //     "The Titicaca Water Frog, Telmatobius culeus, is the largest aquatic frog found in Lake Titicaca in Peru and Bolivia. Individuals have been known to weigh almost a kilogram, and are unmistakable with their folds of skin. Those excessive skin folds have lent the species their nickname, Titicaca Scrotum Frog. Unfortunately it is also considered to be an aphrodisiac when mixed with honey, the roots of a local plant, and several other products, and consumed as a “thick shake”",
    // };

    const frogdata = new dogModel(frog);
    frogdata.save(function (err, data) { 
      if (err) return console.error(err)
      console.log("saved frog! ", data)
    })
  }
}

// crawlFrogInfo(wikiUrl)
// crawlData(url);
