import { Request } from 'express';

interface IRequestWithUser extends Request {
    user?: {
        _id: string;
    };
    session?: {
        user: object;
    };
}

export default IRequestWithUser;
