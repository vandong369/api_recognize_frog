import { Response } from 'express';

interface ResponseContext extends Response {
    success?: any;
    error?: any;
}

export default ResponseContext;
